using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ObstacleSettings", menuName = "ScriptableObjects/ObstacleSettings", order = 1)]
public class ObstacleSettings : ScriptableObject
{
    public ObstacleType type;
    public float targetRange;
    public float damageRange;
}
