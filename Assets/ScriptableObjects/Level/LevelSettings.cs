using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelSettings", menuName = "ScriptableObjects/Level", order = 1)]
public class LevelSettings : ScriptableObject
{
    public float quickTimeEventScaleTarget;
    public float quickTimeEventDuration;
    public bool isOnQuickTimeEvent;
    public float obstacleSpeed;
    public float timeElapsed;
    public float maxStamina;
    public float stamina;
    public float staminaRegenRate;
    public float lightIntensity;

    public void CopySettings(LevelSettings levelSettings)
    {
        quickTimeEventScaleTarget = levelSettings.quickTimeEventScaleTarget;
        quickTimeEventDuration = levelSettings.quickTimeEventDuration;
        isOnQuickTimeEvent = levelSettings.isOnQuickTimeEvent;
        obstacleSpeed = levelSettings.obstacleSpeed;
        timeElapsed = levelSettings.timeElapsed;
        maxStamina = levelSettings.maxStamina;
        stamina = levelSettings.stamina;
        staminaRegenRate = levelSettings.staminaRegenRate;
        lightIntensity = levelSettings.lightIntensity;
    }
}
