using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public enum ObstacleType
{
    Rock,
    Spider,
    All
}

[Serializable]
public struct ObstacleSpawnInfo
{
    public ObstacleType type;
    public float deltaTime;
}

[Serializable]
public struct ValueProgression
{
    public float newQteEventDuration;
    public float newQteEventScaleTarget;
    public float newStaminaRegen;
    public float obstacleSpeedMultiplier;
    public float lightIntensityMultiplier;
    public float timeOfEffect;
}


[CreateAssetMenu(fileName = "BaseLevelSettings", menuName = "ScriptableObjects/BaseLevel", order = 1)]
public class BaseLevelSettings : LevelSettings
{
    [SerializeField]
    public ObstacleSpawnInfo[] spawnInfo;
    [SerializeField]
    public ValueProgression[] valueProgressions;
}
