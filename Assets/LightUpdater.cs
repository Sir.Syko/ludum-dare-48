using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class LightUpdater : MonoBehaviour
{
    [SerializeField]
    private LevelSettings levelSettings;

    private float lightIntensity;
    private Light2D lightComponent;

    private void Start()
    {
        lightComponent = GetComponent<Light2D>();
        lightIntensity = lightComponent.intensity;
    }

    private void Update()
    {
        lightComponent.intensity = lightIntensity * levelSettings.lightIntensity;
    }
}
