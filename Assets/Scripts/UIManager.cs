using System;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private LevelSettings levelSettings;

    [SerializeField]
    private TMP_Text timerText;

    [SerializeField]
    private RectTransform staminaBar;
    private Vector3 staminaBarScale;

    private void Start()
    {
        staminaBarScale = new Vector3(1, 1, 1);    
    }

    private void OnGUI()
    {
        timerText.text = "Distance: " + Math.Ceiling(levelSettings.timeElapsed * 4.17f) + "m";
        staminaBarScale.x = levelSettings.stamina / levelSettings.maxStamina;
        staminaBar.localScale = staminaBarScale;
    }
}
