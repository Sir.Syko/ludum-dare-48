using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RetryMenu : MonoBehaviour
{
    [SerializeField]
    private LevelSettings levelSettings;

    [SerializeField]
    private TMP_Text titleText;

    private void Start()
    {
        Time.timeScale = 0;    
    }

    private void OnEnable()
    {
        titleText.text = "You Ran: " + Math.Ceiling(levelSettings.timeElapsed * 4.17f) + "m";
    }

    public void ResetGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
