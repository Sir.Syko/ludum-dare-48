using MiscUtil.Collections.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TextProcessor
{
    public static string floatToTimerString(float time)
    {
        string timerString = ((int)Math.Floor(time / 60)).ToString("D2");
        timerString += ":";
        timerString += ((int)Math.Floor(time % 60)).ToString("D2");

        return timerString;
    }
}
