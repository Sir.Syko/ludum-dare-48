using UnityEngine;

public class Player : MonoBehaviour
{
    private Animator animator;

    private void Awake()
    {
        animator = GetComponent<Animator>();    
    }

    public void TakeAction(ObstacleType obstacleType)
    {
        switch (obstacleType)
        {
            case ObstacleType.Rock:
                animator.SetTrigger("Jump");
                break;
            case ObstacleType.Spider:
                animator.SetTrigger("Crouch");
                break;
            case ObstacleType.All:
                break;
            default:
                break;
        }
    }

    public void TakePenalty()
    {
        animator.SetTrigger("Stumble");
    }

    public void ApplySpeedMultiplier(float multiplier)
    {
        animator.SetFloat("SpeedMultiplier", animator.GetFloat("SpeedMultiplier") * multiplier);
    }
}
