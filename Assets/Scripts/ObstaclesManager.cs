using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct ObstaclePrefab
{
    public ObstacleType type;
    public GameObject prefab;
}

public class ObstaclesManager : MonoBehaviour
{
    [SerializeField]
    private Transform topSpawner;

    [SerializeField]
    private Transform bottomSpawner;

    [SerializeField]
    private BaseLevelSettings baseLevelSettings;

    [SerializeField]
    private LevelSettings currentLevelSettings;

    [SerializeField]
    private ObstaclePrefab[] obstaclePrefabs;

    [SerializeField]
    private GameObject retryMenuPrefab;

    private Dictionary<ObstacleType, ObstaclePrefab> obstaclePrefabsIndexed = new Dictionary<ObstacleType, ObstaclePrefab>();
    private List<Obstacle> obstacles = new List<Obstacle>();
    private int currentObstacleIndex = 0;

    private Transform player;
    private Player playerScript;

    private QTEManager qteManager;

    private bool isGameOver = false;
    private int currentProgressionIndex = 0;

    void Start()
    {
        qteManager = new QTEManager(baseLevelSettings.quickTimeEventScaleTarget, 1.0f);
        currentLevelSettings.CopySettings(baseLevelSettings);
        player = GameObject.FindWithTag("Player").transform;
        playerScript = player.GetComponent<Player>();
        for(int i = 0; i < obstaclePrefabs.Length; ++i)
        {
            ObstaclePrefab prefab = obstaclePrefabs[i];
            obstaclePrefabsIndexed.Add(prefab.type, prefab);
        }

        StartCoroutine("SpawnObstacles");
        StartCoroutine("ApplyProgression");
    }

    private IEnumerator SpawnObstacles()
    {
        while(true)
        {
            int obstacleIndex;
            if (currentObstacleIndex >= baseLevelSettings.spawnInfo.Length)
            {
                obstacleIndex = UnityEngine.Random.Range(0, baseLevelSettings.spawnInfo.Length);
            }
            else
            {
                obstacleIndex = currentObstacleIndex;
            }

            ObstacleSpawnInfo spawnInfo = baseLevelSettings.spawnInfo[obstacleIndex];
            yield return new WaitForSeconds(spawnInfo.deltaTime);

            InstantiateObstacle(spawnInfo);
            ++currentObstacleIndex;
        }
    }

    private IEnumerator ApplyProgression()
    {
        while (currentProgressionIndex < baseLevelSettings.valueProgressions.Length)
        {
            ValueProgression progression = baseLevelSettings.valueProgressions[currentProgressionIndex];
            yield return new WaitForSeconds(progression.timeOfEffect);

            currentLevelSettings.quickTimeEventDuration = progression.newQteEventDuration;
            currentLevelSettings.quickTimeEventScaleTarget = progression.newQteEventScaleTarget;
            currentLevelSettings.staminaRegenRate = progression.newStaminaRegen;
            currentLevelSettings.obstacleSpeed *= progression.obstacleSpeedMultiplier;
            currentLevelSettings.lightIntensity *= progression.lightIntensityMultiplier;
            playerScript.ApplySpeedMultiplier(progression.obstacleSpeedMultiplier);

            ++currentProgressionIndex;
        }
    }

    private void InstantiateObstacle(ObstacleSpawnInfo obstacleInfo)
    {
        Transform parent;
        switch (obstacleInfo.type)
        {
            case ObstacleType.Rock:
                parent = bottomSpawner;
                break;
            case ObstacleType.Spider:
                parent = topSpawner;
                break;
            default:
                parent = bottomSpawner;
                break;
        }

        GameObject obstacleObject = Instantiate(obstaclePrefabsIndexed[obstacleInfo.type].prefab, parent);
        Obstacle obstacle = obstacleObject.GetComponentInChildren<Obstacle>();


        obstacle.Init(currentLevelSettings);
        obstacles.Add(obstacle);
        StartCoroutine("DestroyObstacle", obstacle);
    }

    private IEnumerator DestroyObstacle(Obstacle obstacle)
    {
        yield return new WaitForSeconds(5);

        obstacles.Remove(obstacle);
        Destroy(obstacle.gameObject);
    }

    void FixedUpdate()
    {
        if (isGameOver) return;

        CheckForQTE();
        currentLevelSettings.isOnQuickTimeEvent = qteManager.UpdateDuration(Time.fixedDeltaTime);
        if(currentLevelSettings.isOnQuickTimeEvent)
        {
            Time.timeScale = qteManager.GetValue();
        }
    }

    private void Update()
    {
        if (isGameOver) return;

        currentLevelSettings.timeElapsed += Time.deltaTime;

        if (Input.GetButtonDown("Fire1")) 
        {
            if(currentLevelSettings.isOnQuickTimeEvent)
            {
                if (qteManager.CanPlayerTakeAction)
                {
                    playerScript.TakeAction(qteManager.GetTriggerObstacle());
                    qteManager.CanPlayerTakeAction = false;
                }
            }
            else
            {
                PlayerPenalty();
            }
        }

        if(currentLevelSettings.stamina < currentLevelSettings.maxStamina)
        {
            currentLevelSettings.stamina += currentLevelSettings.staminaRegenRate * Time.deltaTime;
            if(currentLevelSettings.stamina > currentLevelSettings.maxStamina)
            {
                currentLevelSettings.stamina = currentLevelSettings.maxStamina;
            }
        }
    }

    private void PlayerPenalty()
    {
        currentLevelSettings.stamina -= 20;

        playerScript.TakePenalty();

        if(currentLevelSettings.stamina <= 0)
        {
            currentLevelSettings.stamina = 0;
            GameOver();
        }
    }

    private void CheckForQTE()
    {
        foreach (Obstacle obstacle in obstacles)
        {
            if(obstacle.IsTargetInRangeOfDamage(player) && qteManager.CanPlayerTakeAction)
            {
                Debug.DrawRay(obstacle.transform.position, player.transform.position - obstacle.transform.position, Color.red, 0.2f);
                GameOver();
                obstacles.Remove(obstacle);
                break;
            }
            
            if (obstacle.ShouldQTE && obstacle.IsTargetInRangeOfEvent(player))
            {
                Debug.DrawRay(obstacle.transform.position, player.transform.position - obstacle.transform.position, Color.cyan, 0.2f);
                PerformQTE(obstacle.Type());
                obstacle.ShouldQTE = false;
                break;
            }
        }
    }

    private void GameOver()
    {
        isGameOver = true;
        Instantiate(retryMenuPrefab);
    }

    private void PerformQTE(ObstacleType type)
    {
        qteManager.StartQTE(currentLevelSettings.quickTimeEventDuration, type);
    }
}
