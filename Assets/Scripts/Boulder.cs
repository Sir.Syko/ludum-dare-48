using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boulder : MonoBehaviour
{
    [SerializeField]
    private int rotateSpeed;

    void Update()
    {
        transform.Rotate(transform.forward, -rotateSpeed *Time.deltaTime);
    }
}
