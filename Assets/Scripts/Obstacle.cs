using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [SerializeField]
    private ObstacleSettings settings;

    private Vector3 moveVector;

    public bool ShouldQTE { get; set; }

    public void Init(LevelSettings levelSettings)
    {
        moveVector = new Vector3(-levelSettings.obstacleSpeed, 0, 0);
        ShouldQTE = true;
    }

    public ObstacleType Type() { return settings.type; }

    public bool IsTargetInRangeOfEvent(Transform targetTransform)
    {
        return Vector3.Magnitude(targetTransform.position - transform.position) <= settings.targetRange;
    }

    public bool IsTargetInRangeOfDamage(Transform targetTransform)
    {
        return Math.Abs(targetTransform.position.x - transform.position.x) <= settings.damageRange;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.Translate(moveVector * Time.fixedDeltaTime);
    }
}
