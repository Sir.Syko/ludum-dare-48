using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QTEManager
{
    private float start;
    private float end;

    private float duration;
    private float currentDuration;

    private ObstacleType triggerObstacle;

    public bool CanPlayerTakeAction { get; set; }

    public QTEManager(float startValue, float endValue)
    {
        start = startValue;
        end = endValue;
    }

    public void StartQTE(float newDuration, ObstacleType obstacleType)
    {
        duration = newDuration;
        currentDuration = 0;
        triggerObstacle = obstacleType;
        CanPlayerTakeAction = true;
    }

    public bool UpdateDuration(float deltaTime)
    {
        currentDuration += deltaTime;

        if (currentDuration > duration)
        {
            currentDuration = duration;
            return false;
        }

        return true;
    }

    public float GetValue()
    {
        return start + (end - start) * (currentDuration / duration);
    }

    public ObstacleType GetTriggerObstacle() { return triggerObstacle; }
}
